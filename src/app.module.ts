import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MathModule } from './modules/math/math.module';
import { LoginService } from './modules/login/login.service';
import { LoginModule } from './modules/login/login.module';

@Module({
  imports: [MathModule, LoginModule, ],
  controllers: [AppController, ],
  providers: [AppService, LoginService, ],
})
export class AppModule {}
