import { Controller, Post, Body, Param, Get } from '@nestjs/common';
import { MathService } from './math.service';


@Controller('math')
export class MathController {

    constructor(private readonly mathService: MathService){}

@Post('/add')
async addition(@Body() body: any){
    return this.mathService.add(body['x'], body['y']);
}

@Get('/multiply/:x/:y')
async multiply(@Param('x') x: number,@Param('y') y: number,  ){
    return this.mathService.multiply(x,y);
}

}
