import { Injectable } from '@nestjs/common';

@Injectable()
export class MathService {

    add(x: number, y: number) {
        return { "result": x + y };
    }

    multiply(x: number, y: number) {
        return { "result": x * y };
    }


}
