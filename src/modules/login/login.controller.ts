import { Controller, Post, Body } from '@nestjs/common';
import { LoginService } from './login.service';

@Controller('login')
export class LoginController {
constructor(private readonly loginService: LoginService){}

@Post('')
login(@Body() body: any){
    console.log(body);
    return this.loginService.login(body['username'], body['password']);
}

}
